#!/usr/bin/python
#JSON Parser
import time
import os
import sys
import json
import logging
import re
from types import *

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    logger = logging.getLogger('easyjs')
    sys.argv.reverse()
    sys.argv.pop()

    parserTerm = sys.argv.pop()
    if not re.match("^((([a-zA-Z0-9]|\\\\.|_|-)*)*(\[(\d)*(,(\s)*(\d)+)?\])?(\.)?)*(\.\*)?$", parserTerm):
        logging.error("easyjs.main: parserTerm contains invalid characters")
        exit(1)

    jsonfile = ""
    if len(sys.argv) == 0:
        logger.debug("easyjs.main():Entering pipe mode.\n")
        filehandle = sys.stdin
    elif len(sys.argv) == 1:
        logger.debug("easyjs.main():Entering file mode.\n")
        filename = sys.argv.pop()
        if (os.path.isfile(filename)):
            filehandle = open(filename, 'r')
        else:
            print("No file " + filename + "found. \n")
            exit(1)
    else:
        logger.error("easyjs.main(): Wrong usage of easyjs.")
    jsonfile = filehandle.readlines()
    jsonfile = "".join(jsonfile)
    filehandle.close()
    jsonobject = json.loads(jsonfile)

    traverseJson(jsonobject, parserTerm)

#function traverseJson traverses the jsonObject depth search first style.
#@param <pyhtonJsonRep> jsonObj     - is the jsonObject to traverses in python reprensentation
#@param <string> traverseTerm       - defines how far to procede with the search
def traverseJson(jsonObj, traverseTerm):
    #Traverse term is split on '.'; first element in the array is the next element to traverse the json
    #"(?<!\\\\)\." expression is used to allow escaping of '.'
    termList = re.split("(?<!\\\\)\.", traverseTerm)
    termList.reverse()
    currentTerm = termList.pop()
    #Parse current term, check for list index '[]' and choice grouping '()'
    listMatch = re.match("(([a-zA-Z0-9]|_|-)+)(\[((.)*)\])", currentTerm)
    rangeDefined = False
    if listMatch:
        rangeDefined = True
        currentTerm = listMatch.group(1)
        currentRange = listMatch.group(4)
        if len(currentRange) > 0:
            currentRange = re.split(",", currentRange)
            rangeA = currentRange[0].strip()
            rangeB = rangeA
            if len(currentRange) > 1:
                rangeB = currentRange[1].strip()
    termList.reverse()
    if rangeDefined:
        counter = 0
        if currentTerm in jsonObj and isinstance(jsonObj[currentTerm], list):
            for element in jsonObj[currentTerm]:
                if  not rangeDefined or counter in range(int(rangeA), int(rangeB)+1):
                    traverseJson(element, ".".join(termList))
                counter = counter + 1
        else:
            print("easyjs.traverseJson: No item available " + currentTerm)

    elif currentTerm == "*":
        print(json.dumps(jsonObj))
    elif currentTerm == "":
        if isinstance(jsonObj, dict):
            for key in jsonObj:
                print(key + ": " + str(type(jsonObj[key])))
        elif isinstance(jsonObj, list):
            counter = 0
            for element in jsonObj:
                if  not rangeDefined or counter in range(int(rangeA), int(rangeB)+1):
                    traverseJson(element, "")
                counter = counter + 1
        else:
            print(str(jsonObj))
    else:
        if isinstance(jsonObj, list):
            counter = 0
            for element in jsonObj:
                if  not rangeDefined or counter in range(int(rangeA), int(rangeB)+1):
                    traverseJson(element, traverseTerm)
                counter = counter + 1
        elif isinstance(jsonObj, dict):
            if currentTerm in jsonObj:
              traverseJson(jsonObj[currentTerm], ".".join(termList))
            else:
              print("easyjs.traverseJson: No item available " + currentTerm)
        else:
            print("easyjs.traverseJson: No item available " + currentTerm)

main()
