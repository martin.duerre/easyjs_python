## Synopsis

This scripts aims at providing an easier way to work with json files from the command line.
Json documents are traversed element by element, this way it is not required to understand the entire structure of a json document before parsing it.

## Usage
The script can be run in two different modes  
**Pipe mode**
```
<some_file_pipe> | easyjs.py "<parserTerm>"
```

**Argument mode**
```
easyjs.py "<parserTerm>" <filename>
```

### parserTerm

Where *parserTerm* is a '.' separated list of elements starting from the root of the json document:

  **""** - Returns the root elements  
  **"RootElement"** - Enters an element below the root called "RootElement"  
  **"RootElement.SubElement"** - Traverses to a subelement of the "RootElement"  

### Json Representation
To return the **json reprensentation** of an given element append '.*' to the element.

  **"RootElement.SubElement.*"** - Return the json representation of the subelement.

### Lists

For working with lists an array like syntax is supported.

   If an subelement points to a list it is possible to index the list appending **[intA,intB]** to the element.
   Only elements within the range of intA to intB will be traversed. If ,intB is obliterated  only the element at position intA is traversed.
   The index starts at 0.
   If no indexes are used all elements in the list will be traversed.

**"RootElement.SubElement.ListElement[3,6]"** - Traverses list subelements 3,4,5,6.

## Example

Consider the following JSON file called sample.json (This file can be found within this repository):
```
{
  "Owner": {
    "Name": "Martin",
    "Phone": 123,
    "LuckyNumbers": [1,2,3]
  },
  "Contacts":
    [
      {
        "Name": "Uwe",
        "Phone": 456,
        "LuckyNumbers": [4,5,6]
      },
      {
        "Name": "Stephan",
        "Phone": 789,
        "LuckyNumbers": [7,8,9]
      },
      {
        "Name": "Holger",
        "Phone": 101112,
        "LuckyNumbers": [10,11,12]
      }
    ]
}
```

Start the parsing
```
easyjs.py "" sample.json
```
will retrun the following output
```
Contacts: <class 'list'>
Owner: <class 'dict'>
```

To access the child elements use:
```
easyjs.py "Owner" sample.json
```
which returns
```
Phone: <class 'int'>
LuckyNumbers: <class 'list'>
Name: <class 'str'>
```

And finally
```
easyjs.py "Owner.Phone" sample.json
```
to access the final child element
```
123
```

**Json reprensentation**
```
peasyjs.py "Owner.*" sample.json
```
returns the json presentation of the Owner Element
```
{"Name": "Martin", "Phone": 123, "LuckyNumbers": [1, 2, 3]}
```

**Accessing Lists**
```
easyjs.py "Contacts[1].LuckyNumbers[0,1]" sample.json
```
returns the first two lucky numbers of the second contact
```
7
8
```

## Motivation

Working with json files from a (non-powershell) cli can be quite enerving.
The 'jq' command line tool, which provides a similar syntax as easyjs.py, always works with the json representation of a document.
If the document is big and complicated it can be difficult to understand its structure and therefore parsing it can take some effort.  

Considering this problem, easyjs offers an step-by-step approach to a)understand and b)parse json documents.


## Installation

Download and run. If python 2 or 3 are installed on your system, no further libraries are required.

## Contributors

Martin Duerre

## License
Licensed under the Apache License, Version 2.0 (the License); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
